#!/bin/bash
S1='LatexTemplate.pdf'
NEWEST=
for f in *.tex *.pdf
do
    if [ -z "$NEWEST" ]
    then 
        NEWEST=$f
    elif [ "$f" -nt "$NEWEST" ] 
    then
        NEWEST=$f
    fi
done
if [ $NEWEST != $S1 ]; 
then
	echo "compile $S1"
	pdflatex  -interaction=batchmode LatexTemplate.tex
	bibtex LatexTemplate
	pdflatex -interaction=batchmode LatexTemplate.tex
	pdflatex -interaction=batchmode LatexTemplate.tex
else 
 echo "no need to compile $S1"
fi